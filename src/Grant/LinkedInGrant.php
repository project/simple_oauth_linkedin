<?php

namespace Drupal\simple_oauth_linkedin\Grant;

use Drupal\Core\Session\AccountInterface;
use Drupal\simple_oauth\Entities\UserEntity;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\social_auth_linkedin\LinkedInAuthManager;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Server\Entities\ClientEntityInterface;
use League\OAuth2\Server\Entities\UserEntityInterface;
use League\OAuth2\Server\Exception\OAuthServerException;
use League\OAuth2\Server\Grant\AbstractGrant;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use League\OAuth2\Server\RequestEvent;
use League\OAuth2\Server\ResponseTypes\ResponseTypeInterface;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Password grant class.
 */
class LinkedInGrant extends AbstractGrant {

  /**
   * The LinkedIn Auth Manager.
   *
   * @var \Drupal\social_auth_linkedin\LinkedInAuthManager
   */
  protected $linkedInAuthManger;

  /**
   * The Network Manager.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  protected $networkManager;

  /**
   * The Social Auth User Authenticator.
   *
   * @var \Drupal\social_auth\User\UserAuthenticator
   */
  protected $userAuthenticator;

  /**
   * The Current User.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Construct LinkedInGrant object.
   *
   * @param \League\OAuth2\Server\Repositories\UserRepositoryInterface $userRepository
   *   The Oauth User Repository.
   * @param \League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface $refreshTokenRepository
   *   The Refresh Token Repository.
   * @param \Drupal\social_auth_linkedin\LinkedInAuthManager $linkedInAuthManager
   *   The LinkedIn Auth Manager.
   * @param \Drupal\social_api\Plugin\NetworkManager $networkManager
   *   The Network Manager.
   * @param \Drupal\social_auth\User\UserAuthenticator $userAuthenticator
   *   The Social Auth User Authenticator.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The Current User.
   */
  public function __construct(UserRepositoryInterface $userRepository,
    RefreshTokenRepositoryInterface $refreshTokenRepository,
    LinkedInAuthManager $linkedInAuthManager,
    NetworkManager $networkManager,
    UserAuthenticator $userAuthenticator,
    AccountInterface $currentUser) {

    $this->setUserRepository($userRepository);
    $this->setRefreshTokenRepository($refreshTokenRepository);
    $this->linkedInAuthManger = $linkedInAuthManager;
    $this->networkManager = $networkManager;
    $this->userAuthenticator = $userAuthenticator;
    $this->currentUser = $currentUser;

    $this->refreshTokenTTL = new \DateInterval('P1M');
  }

  /**
   * {@inheritdoc}
   */
  public function respondToAccessTokenRequest(ServerRequestInterface $request,
    ResponseTypeInterface $responseType,
    \DateInterval $accessTokenTTL) {

    // Validate request.
    $client = $this->validateClient($request);
    $scopes = $this->validateScopes($this->getRequestParameter('scope', $request, $this->defaultScope));
    $user = $this->validateUser($request, $client);

    // Finalize the requested scopes.
    $finalizedScopes = $this->scopeRepository->finalizeScopes($scopes, $this->getIdentifier(), $client, $user->getIdentifier());

    // Issue and persist new access token.
    $accessToken = $this->issueAccessToken($accessTokenTTL, $client, $user->getIdentifier(), $finalizedScopes);
    $this->getEmitter()->emit(new RequestEvent(RequestEvent::ACCESS_TOKEN_ISSUED, $request));
    $responseType->setAccessToken($accessToken);

    // Issue and persist new refresh token if given.
    $refreshToken = $this->issueRefreshToken($accessToken);

    if ($refreshToken !== NULL) {
      $this->getEmitter()->emit(new RequestEvent(RequestEvent::REFRESH_TOKEN_ISSUED, $request));
      $responseType->setRefreshToken($refreshToken);
    }

    return $responseType;
  }

  /**
   * @param \Psr\Http\Message\ServerRequestInterface $request
   * @param \League\OAuth2\Server\Entities\ClientEntityInterface $client
   *
   * @return \League\OAuth2\Server\Entities\UserEntityInterface
   *   Oauth User Entity.
   *
   * @throws \League\OAuth2\Server\Exception\OAuthServerException
   */
  protected function validateUser(ServerRequestInterface $request, ClientEntityInterface $client) {
    $linkedin_access_token = $this->getRequestParameter('linkedin_access_token', $request);

    if (\is_null($linkedin_access_token)) {
      throw OAuthServerException::invalidRequest('linkedin_access_token');
    }

    try {
      $this->linkedInAuthManger->setAccessToken(new AccessToken([
        'access_token' => $linkedin_access_token,
      ]));
      /** @var \League\OAuth2\Client\Provider\AbstractProvider|false $client */
      $client = $this->networkManager->createInstance('social_auth_linkedin')
        ->getSdk();
      $this->linkedInAuthManger->setClient($client);
      /** @var \League\OAuth2\Client\Provider\LinkedInResourceOwner $profile */
      $profile = $this->linkedInAuthManger->getUserInfo();
      if (empty($profile)) {
        throw OAuthServerException::invalidCredentials();
      }

      $data = $this->userAuthenticator->checkProviderIsAssociated($profile->getId()) ? NULL : $this->linkedInAuthManger->getExtraDetails();
      $profile_name = $profile->getFirstName() . " " . $profile->getLastName();
      $this->userAuthenticator->authenticateUser($profile_name,
        $profile->getEmail(),
        $profile->getId(),
        $this->linkedInAuthManger->getAccessToken(),
        $profile->getImageUrl(),
        $data);
    }
    catch (\Exception $exception) {
      throw new OAuthServerException(
        $exception->getMessage(),
        $exception->getCode(),
        'invalid_linkedin_credentials',
        400
      );
    }

    if (!$this->currentUser->isAuthenticated()) {
      throw OAuthServerException::invalidCredentials();
    }
    $user = new UserEntity();
    $user->setIdentifier($this->currentUser->id());

    return $user;
  }

  /**
   * {@inheritdoc}
   */
  public function getIdentifier() {
    return 'linkedin_login_grant';
  }

}
