<?php

namespace Drupal\simple_oauth_linkedin\Plugin\Oauth2Grant;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\simple_oauth\Plugin\Oauth2GrantBase;
use Drupal\simple_oauth_linkedin\Grant\LinkedInGrant;
use Drupal\social_api\Plugin\NetworkManager;
use Drupal\social_auth\User\UserAuthenticator;
use Drupal\social_auth_linkedin\LinkedInAuthManager;
use League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface;
use League\OAuth2\Server\Repositories\UserRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * @Oauth2Grant(
 *   id = "linkedin_login_grant",
 *   label = @Translation("LinkedIn Login Grant")
 * )
 */
class LinkedIn extends Oauth2GrantBase {

  /**
   * The League Oauth2 User Repository.
   *
   * @var \League\OAuth2\Server\Repositories\UserRepositoryInterface
   */
  protected $userRepository;

  /**
   * The League Oauth2 Refresh Token Repository.
   *
   * @var \League\OAuth2\Server\Repositories\RefreshTokenRepositoryInterface
   */
  protected $refreshTokenRepository;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The LinkedIn Auth Manager.
   *
   * @var \Drupal\social_auth_linkedin\LinkedInAuthManager
   */
  protected $linkedInAuthManger;

  /**
   * The Network Manager.
   *
   * @var \Drupal\social_api\Plugin\NetworkManager
   */
  protected $networkManager;

  /**
   * The Social Auth User Authenticator.
   *
   * @var \Drupal\social_auth\User\UserAuthenticator
   */
  protected $userAuthenticator;

  /**
   * The Current User.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Class constructor.
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    UserRepositoryInterface $user_repository,
    RefreshTokenRepositoryInterface $refresh_token_repository,
    ConfigFactoryInterface $config_factory,
    LinkedInAuthManager $linkedInAuthManager,
    NetworkManager $networkManager,
    UserAuthenticator $userAuthenticator,
    AccountInterface $currentUser) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->userRepository = $user_repository;
    $this->refreshTokenRepository = $refresh_token_repository;
    $this->configFactory = $config_factory;
    $this->linkedInAuthManger = $linkedInAuthManager;
    $this->networkManager = $networkManager;
    $this->userAuthenticator = $userAuthenticator;
    $this->currentUser = $currentUser;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('simple_oauth.repositories.user'),
      $container->get('simple_oauth.repositories.refresh_token'),
      $container->get('config.factory'),
      $container->get('social_auth_linkedin.manager'),
      $container->get('plugin.network.manager'),
      $container->get('social_auth.user_authenticator'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Exception
   */
  public function getGrantType() {
    $grant = new LinkedInGrant($this->userRepository,
      $this->refreshTokenRepository,
      $this->linkedInAuthManger,
      $this->networkManager,
      $this->userAuthenticator,
      $this->currentUser);

    $settings = $this->configFactory->get('simple_oauth.settings');
    $grant->setRefreshTokenTTL(new \DateInterval(sprintf('PT%dS', $settings->get('refresh_token_expiration'))));
    return $grant;
  }

}
